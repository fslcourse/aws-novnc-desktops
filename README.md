# FSL Course Desktops

_Ubuntu 22.04 + noVNC + XFCE + nginx + websockify_

The FSL Course is held annually. The course is run by the members of the Analysis group at the University of Oxford (FMRIB). The course is aimed at researchers and clinicians who are interested in learning about FSL tools and techniques for analysing neuroimaging data.

## Why cloud desktops?

The FSL course is a hands-on course where participants are expected to follow along with the course material on computers.

### The past

In the past, the course organisers would rent laptops and other computing equipment to host the course. This method allowed the course organisers to ensure that all participants had the necessary software and data to follow along with the course material. Data and software distribution was done using USB sticks and hard drive cloning. The data and software required for the course is nearly 100GB, which makes downloading on personal laptops during a course impractical on shared wifi networks.

The Covid-19 pandemic forced the course organisers to move the course online.

### Workspaces

The first cloud desktops for an online FSL course used AWS Workspaces. Workspaces were time consuming to provision and as a managed service, they were more expensive than they needed to be.

One limitation of Workspaces included blocked network ports required by the workspaces client, which made it difficult for some participants to connect to the desktops. Over the years, customisation of the desktop environment was limited by the managed nature of the service and opaque provisioning process that made it difficult to debug issues.

The FSL course used workspaces for three years during two online courses and one in-person course. Even during the in-person course, cloud desktops proved to have more advantages than the old way.

### noVNC on AWS EC2

The current cloud desktops for the FSL course use noVNC on AWS EC2. noVNC is a web-based VNC client that allows users to connect to a remote desktop using a web browser.

Advandages of noVNC on EC2 over workspaces include:
- Cost savings (bare ec2 instances are cheaper than managed workspaces)
- Customisation (full control over the desktop environment)
- Debugging (full control over the provisioning process)
- Network ports (no blocked ports since noVNC uses HTTP/HTTPS over port 80/443 via the browser)
- No special client software required (noVNC works in the browser)
- Scalability (easy to provision hundreds of desktops for a course)
- Time savings (provisioning is automated after initial setup of the main AMI)

## How it works

An AMI (Amazon Machine Image) is manually created with the necessary software and data required for a course.

[Details on creating an AMI](./docs/manual_setup.md)

The AMI is then used as the base image when provisioning cloud desktops with terraform.

[Details on using terraform](./docs/terraform.md)

## License

This project is licensed under the Apache License, Version 2.0 - see the [LICENSE](LICENSE) file for details.
