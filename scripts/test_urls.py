# the necessary csv file looks like this:
# user,machine
# username1,http://ec2-3-129-210-51.us-east-2.compute.amazonaws.com
# username2,http://ec2-18-216-134-166.us-east-2.compute.amazonaws.com

import csv
import http.client
from urllib.parse import urlparse

def test_url_status(username, url):
    # Parse the URL to extract the domain and path
    url_parsed = urlparse(url)
    host = url_parsed.netloc
    path = url_parsed.path
    if not path:
        path = '/'
    if url_parsed.query:
        path += '?' + url_parsed.query

    # Determine the connection type (HTTP or HTTPS)
    # The FSL course could use HTTPS in the future...
    if url_parsed.scheme == 'https':
        connection = http.client.HTTPSConnection(host)
    else:
        connection = http.client.HTTPConnection(host)

    try:
        # Send a GET request to the URL
        connection.request("GET", path)
        response = connection.getresponse()
        # only print the non 200 status codes
        # a 502 usually means that the websockify server is not running
        # so nginx can't proxy the request to the websockify server
        # There appears to be an intermittent issue where the websockify server
        # is not started after the vnc server is started
        if response.status != 200:
            print(f"{response.status} - {username} -  {url}")
    except Exception as e:
        print(f"Error accessing {url}: {e}")
    finally:
        connection.close()

# read the csv file rows
# data = []
# with open('user_machine_mapping.csv', 'r') as file:
#     reader = csv.reader(file)
#     for row in reader:
#         data.append(row)

# for each url, test that it returns a 200 status code
# for row in data[1:]:
    # test_url_status(row[0], row[1])
    #
    #
    #
# make urls that look like this: https://test_1.fslcourse.com
# the test number range from 1 to 99 with no leading zeros (only used for infastucture testing)
urls = []
for i in range(1, 100):
    urls.append(f"http://test_{i}.fslcourse.com/?password=FSLCourse2024!")

for url in urls:
    test_url_status(url, url)
