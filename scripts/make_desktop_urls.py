#!/usr/bin/env fslpython

# read the csv file from the Oxford online shop,
# and make new columns for the desktop urls and desktop passwords.
# Save the new data as a csv file with the current date and time

import pandas as pd
import datetime
import sys
import os

# tutors is a list of tutors that should be included in the
# desktop urls and passwords.
tutors = [
    'Taylor Hanayik',
    'Ludovica Griffanti',
    'Janine Bijsterbosch',
    'Michiel Cottaar',
    'Rezvan Farahibozorg',
    'Joe Kong',
    'Grace Gillis',
    'Fidel Alfaro-Almagro',
    'Rick Lange',
    'Kiotaka Nemoto',
    'Masaki Fukunaga',
    'Marieke Martens',
    'Shaun Warrington',
    'Matthew Webster',
    'Paul McCarthy',
    'Kaitlin Krebs'
]


def main():
    '''
    do all the work
    '''
    # get the file from the command line arguments
    if len(sys.argv) < 2:
        print('Usage: python make_desktop_urls.py <input_file.csv>')
        sys.exit(1)

    input_file = sys.argv[1]
    if not os.path.exists(input_file):
        print('File not found:', input_file)
        sys.exit(1)

    # read the csv file
    df = pd.read_csv(input_file)

    # make sure there are no weird characters in the cells
    df = df.astype(str)
    df = df.map(lambda x: x.encode('unicode_escape').decode('utf-8'))

    # add the tutors names to the name column
    # this is necessary because the tutors are not in the csv file
    # but they should be included in the desktop urls and passwords
    df = df._append(pd.DataFrame({'Delegate Name': tutors}), ignore_index=True)

    # make the new column: desktop_url
    # each url is created from the Name column, and we need to remove all leading and trailing spaces.
    # A name such as "Taylor Hanayik" becomes "taylor_hanayik" + the url pattern.
    # so, "Taylor Hanayik" becomes "https://taylor_hanayik.fslcourse.com"
    df['desktop_url'] = 'https://' + df['Delegate Name'].str.lower().str.strip().str.replace(' ', '_') + '.fslcourse.com'

    # the password is the same for all: FSLcourse2024!
    df['desktop_password'] = 'FSLcourse2024!'

    # verify that there are no duplicates in the desktop_url column
    # if there are, raise an error
    if df['desktop_url'].duplicated().any():
        print('Error: there are duplicates in the desktop_url column')
        sys.exit(1)

    # save the data to a new file
    now = datetime.datetime.now()
    output_file = 'fslcourse_badge_info_' + now.strftime('%Y%m%d_%H%M%S') + '.csv'
    df.to_csv(output_file, index=False)



if __name__ == '__main__':
    main()
