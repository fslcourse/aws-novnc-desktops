import json

def generate_test_users(count):
    users = []
    for i in range(1, count + 1):
        user = {
            "name": f"test_{i}",
            "username": f"test_{i}",
            "email": "",
            "region": "ap-northeast-3",
            "ami": "ami-02b429b450838db30"
        }
        users.append(user)
    return users

# Generate 100 users
users = generate_test_users(100)

# Save the JSON output to a file
with open('users.json', 'w') as f:
    json.dump(users, f, indent=2)

print('Users saved to users.json')
