module "s3_bucket_terraform_state" {
  source                      = "../s3_terraform_state"
  terraform_state_bucket_name = "fslcourse-terraform-state-bucket"
}

module "dynamodb_terraform_state" {
  source                               = "../dynamodb_terraform_state"
  fslcourse_terraform_state_table_name = "fslcourse-terraform-state"
  deletion_protection_enabled          = false
}
