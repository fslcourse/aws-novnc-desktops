# DynamoDB table for storing Terraform state lock for the FSL application
resource "aws_dynamodb_table" "fslcourse_terraform_state_locks" {
  name           = var.fslcourse_terraform_state_table_name
  hash_key       = "LockID"
  billing_mode   = "PAY_PER_REQUEST"

  deletion_protection_enabled = var.deletion_protection_enabled

  attribute {
    name = "LockID"
    type = "S"
  }
}