variable "fslcourse_terraform_state_table_name" {
  description = "Name of the DynamoDB table for storing FSL course Terraform state lock"
  type        = string
  default     = "fslcourse-terraform-state"
}


variable "deletion_protection_enabled" {
  description = "Flag to control whether to prevent destruction of DynamoDB tables with data"
  type        = bool
  default     = false
}

