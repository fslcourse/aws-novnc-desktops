terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"

  # Configure the backend to use S3 and DynamoDB for storing the Terraform state
  # This allows multiple users to work on the same infrastructure because state is stored remotely
  backend "s3" {
    bucket         = "fslcourse-terraform-state-bucket"
    key            = "terraform/state.tfstate"
    region         = "eu-west-2"
    encrypt        = true
    dynamodb_table = "fslcourse-terraform-state"
  }
}

locals {
  users = jsondecode(file("users.json"))
}

# private_key_path is a variable that will be used
# to pass the path to the SSH private key.
# This way, the private key can be kept out of the
# configuration file and not be exposed to others.
variable "private_key_path" {
  description = "Path to the SSH private key"
  type        = string
}

variable "key_name" {
  description = "Name of the SSH key pair"
  type        = string
}

# CHANGE THE REGION TO THE ONE YOU WANT TO USE
provider "aws" {
  region = "ap-northeast-3" # Osaka
}

# Create an IAM role
resource "aws_iam_role" "ssm_role" {
  name = "EC2SSMRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

# Attach the AmazonEC2RoleforSSM policy to the IAM role
resource "aws_iam_role_policy_attachment" "ssm_role_policy" {
  role       = aws_iam_role.ssm_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

# Attach the role to the EC2 instance profile
resource "aws_iam_instance_profile" "ssm_instance_profile" {
  name = "EC2SSMInstanceProfile"
  role = aws_iam_role.ssm_role.name
}

resource "aws_instance" "fsl_desktop" {
  # use a for_each loop to create an instance
  # for each user in the users.json file
  for_each = { for user in local.users : user.username => user }

  # set some common attributes for the instances
  ami                  = each.value.ami #"ami-02b429b450838db30" # fslcourse2024v4 osaka
  instance_type        = "t2.large"
  subnet_id            = "subnet-0f94a4cd351f9a4d1" # fsl desktops osaka
  security_groups      = ["sg-0599dd5d9776c7245"]   # fsl desktops osaka
  iam_instance_profile = aws_iam_instance_profile.ssm_instance_profile.name
  tags = {
    Name = each.value.username
  }

  key_name  = var.key_name         # must have matching pem file on your system (like fsl-desktop-osaka)
  user_data = file("user_data.sh") # Reads the content of the user_data.sh file to provision the instance

  # prevent the instance from being destroyed in some cases
  # this is useful when you want to keep instances running
  # while adding new users or updating the configuration
  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      tags,
      user_data,
      ami,
      instance_type,
      subnet_id,
      security_groups
    ]
  }

  # use ssh to connect to the instance
  # and automatically start the VNC server for us
  provisioner "remote-exec" {
    when = create
    inline = [
      # avoid the Authentication Required to Create Managed Color Device error
      "sudo rm /usr/share/polkit-1/actions/org.freedesktop.color.policy",
      # start the VNC server
      "/bin/bash /home/ubuntu/.startvnc.sh",
      # sleep and then run the .startvnc.sh script again
      # this may help to ensure that the websockify server is started
      "sleep 20",
      # check if the websockify server is running on port 6080
      "w=$(lsof -ti tcp:6080)",
      # if w is empty, then the websockify server is not running
      # so we start it again (the vnc server will not restart if it is already running)
      "if [ -z $w ]; then /bin/bash /home/ubuntu/.startvnc.sh; fi",
      # diable the unsafe paste warning in the default terminal
      "sed -i 's/MiscShowUnsafePasteDialog=TRUE/MiscShowUnsafePasteDialog=FALSE/' /home/ubuntu/.config/xfce4/terminal/terminalrc",
      # set the firefox home page
      "echo 'user_pref(\"browser.startup.homepage\", \"https://open.win.ox.ac.uk/pages/fslcourse/website/online_materials.html\");' >> ~/.mozilla/firefox/43gaxf8y.default-release/prefs.js"
    ]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.private_key_path)
      host        = self.public_ip
    }
  }

}

resource "aws_route53_record" "instance_dns_records" {
  for_each = { for user in local.users : user.username => user }

  zone_id = "Z00565473CX2W4U2FK9CK"
  name    = "${each.value.username}.fslcourse.com"
  type    = "CNAME"
  ttl     = "30"
  records = [aws_instance.fsl_desktop[each.value.username].public_dns]
}

# Output block for EC2 instance DNS and Name
output "instance_dns_and_names" {
  value       = { for instance in aws_instance.fsl_desktop : instance.tags["Name"] => instance.public_dns }
  description = "Mapping of EC2 instance names to their public DNS values."
}
