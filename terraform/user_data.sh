#!/bin/bash
# Check if the 'ubuntu' user exists, if not, create it
if ! id -u ubuntu > /dev/null 2>&1; then
  useradd -m -s /bin/bash ubuntu
  echo 'ubuntu:ubuntu' | chpasswd
fi
# Add the user to sudoers
echo "ubuntu ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ubuntu

# Define the full path to the xstartup script
XSTARTUP_PATH="/home/ubuntu/.vnc/xstartup"

# Replace the xstartup script with the required commands (fixes xstartup for older AMIs)
cat <<EOF > "$XSTARTUP_PATH"
#!/usr/bin/env bash
# Source the user's profile
. \$HOME/.profile

# Start XFCE4
/usr/bin/startxfce4
EOF

BASHRC_PATH="/home/ubuntu/.bashrc"

# append the following lines to the .bashrc file
cat <<EOF >> "$BASHRC_PATH"
if [ -f ~/.profile ]; then
    . ~/.profile
fi
EOF

PROFILE_PATH="/home/ubuntu/.profile"

# replace the .profile file with the following content
cat <<EOF > "$PROFILE_PATH"
# set PATH so it includes user's private bin if it exists
if [ -d "\$HOME/bin" ] ; then
PATH="\$HOME/bin:\$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "\$HOME/.local/bin" ] ; then
PATH="\$HOME/.local/bin:\$PATH"
fi

# FSL Setup
FSLDIR=/home/ubuntu/fsl
PATH=\${FSLDIR}/share/fsl/bin:\${PATH}
export FSLDIR PATH
. \${FSLDIR}/etc/fslconf/fsl.sh

alias fsleyes="fsleyes --movieSync"

# set default browser to firefox
export BROWSER=/usr/bin/firefox

# Fix: Authorization required, but no authorization protocol specified
xhost + > /dev/null
EOF


# Make sure the xstartup script is executable
chmod +x "$XSTARTUP_PATH"

# clear the history from the bash history file (copied from the master image)
cat /dev/null > /home/ubuntu/.bash_history
