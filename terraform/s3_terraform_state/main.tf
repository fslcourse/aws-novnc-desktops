# Create an S3 bucket for storing Terraform state
resource "aws_s3_bucket" "terraform_state_bucket" {
  bucket = var.terraform_state_bucket_name
}

# Configure S3 Bucket Ownership Controls for the Terraform state bucket
resource "aws_s3_bucket_ownership_controls" "terraform_state_ownership_controls" {
  bucket = aws_s3_bucket.terraform_state_bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

# Set the ACL of the Terraform state bucket to private
resource "aws_s3_bucket_acl" "terraform_state_acl" {
  depends_on = [aws_s3_bucket_ownership_controls.terraform_state_ownership_controls]
  bucket     = aws_s3_bucket.terraform_state_bucket.id
  acl        = "private"
}

# Configure server-side encryption with bucket keys for the Terraform state bucket
resource "aws_s3_bucket_server_side_encryption_configuration" "terraform_state_encryption" {
  bucket = aws_s3_bucket.terraform_state_bucket.id
  rule {
    # Enable bucket key
    bucket_key_enabled = true
  }
}




