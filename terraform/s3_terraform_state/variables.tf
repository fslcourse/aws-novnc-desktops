variable "terraform_state_bucket_name" {
  description = "Name of the S3 bucket for stroing Terraform state for FSL clinical"
  type        = string
  default     = "fslcourse-terraform-state-bucket"
}