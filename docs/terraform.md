# Using Terraform

The `terraform` directory contains the scripts used to provision the desktops automatically using preconfigured AMIs.

## Installing Terraform

For the latest instructions on installing Terraform, see the [official documentation](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli).

## Before deploying with terraform

This terraform configuration uses remote state locking and storage so that multiple people can handle a deployment without stepping on each other's toes (and stay in sync with each other).

For this to work, you need to do the following:

- `cd ./terraform/bootstrap`
- `terraform init`
- `terraform apply`

You only need to run the bootstraping step once. After that, you can deploy the desktops using the terraform scripts. If the bootstrap configuration changes, you will need to run the bootstrap step again though.

## Deploying the desktops

As a rough guide, it takes about 20 minutes to deploy 100 desktops simultaneously. A handful of desktops should take less than 5 minutes.

```bash
cd terraform
terraform init # must be run after the bootstrap step above
terraform apply -var="key_name=fsl-desktop-osaka" -var="private_key_path=/some/key.pem"
```

After deployment the desktops will be available at the following URLs:

`https://<username>.fslcourse.com`

In the example URL above, username is each user's username as specified in `users.json`

`users.json` must have an array of user objects with the following fields:

```
[
  {
    "name": "user_1",
    "username": "user_1",
    "email": "optional@email.com",
    "region": "ap-northeast-3",
    "ami": "ami-02b429b450838db30"
  }
]
```

## To destroy the desktops

```bash
# from the terraform directory (cd terraform if needed)
terraform destroy -var="key_name=fsl-desktop-osaka" -var="private_key_path=/PATH/TO/PRIVATE_KEY.pem"
```
