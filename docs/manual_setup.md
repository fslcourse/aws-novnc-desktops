# Creating an AMI

The following steps are used to manually provision a desktop. This method is useful when testing and modifiying the desktop environment.

Use the terraform scripts in the `terraform` directory to provision a desktop automatically using preconfigured AMIs.

## Launch an Ubuntu 22.04 instance

Launch an Ubuntu 22.04 instance in the AWS web console. The instance type should be `t2.large` (2vCPU 8GB) and the AMI should be the latest Ubuntu 22.04 AMI.

## Install the system dependencies

Connect to the instance using SSH and run the following commands to install the necessary software:

```bash
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get install -q -y xfce4 xfce4-goodies tigervnc-standalone-server dbus-x11 novnc nginx websockify dc
```

## Install Firefox

https://support.mozilla.org/en-US/kb/install-firefox-linux#w_install-firefox-deb-package-for-debian-based-distributions

## Install FSL and the course data

Run the following commands to install FSL and the course data:

```bash
# download the installer
wget https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/fslinstaller.py

# run the installer
python3 fslinstaller.py -d /home/ubuntu/fsl

# source the updated profile (or logout and login again)
source ~/.profile # or logout and login again

# download the course data
fsl_add_module # follow the prompts for "all"
```

## Edit the noVNC configuration

Run the following commands to edit the noVNC configuration:

```bash
# make remote resizing the default
sed -i "s/UI.initSetting('resize', 'off');/UI.initSetting('resize', 'remote');/g" /usr/share/novnc/app/ui.js
```

Update the noVNC html to remove branding and add our own logo

- remove `noVNC_logo` related stuff from `/usr/share/novnc/vnc.html`
- add FSL logo to connect dialog box (put logo asset in `/usr/share/novnc/app/images/`)

## Configure VNC

Run the following commands to configure VNC:

```bash
vim ~/.vnc/xstartup
```

Add the following content to the file:

```bash
#!/usr/bin/env bash
. $HOME/.profile # source the profile for the user
/usr/bin/startxfce4 # starts the desktop environment
```

Run the following commands to make the file executable:

```bash
chmod +x ~/.vnc/xstartup
```

Define some configs for the server

```bash
vim ~/.vnc/config
```

Add the following content to the file:

```bash
geometry=1920x1200
```

## Set the VNC password

Run the following commands to set the VNC password:

```bash
vncpasswd
```

## Create VNC start and stop scripts

Run the following commands to create the VNC scripts:

```bash
touch ~/.startvnc.sh
touch ~/.stopvnc.sh
```

Add the following content to the `~/.startvnc.sh` file:

```bash
#!/usr/bin/bash
/usr/bin/vncserver -localhost no :0
websockify -D --web=/usr/share/novnc/ 6080 localhost:5900
```

Add the following content to the `~/.stopvnc.sh` file:

```bash
#!/usr/bin/bash
kill $(lsof -ti tcp:6080)
/usr/bin/vncserver -kill :0
exit 0
```

Run the following commands to make the scripts executable:

```bash
chmod +x ~/.startvnc.sh
chmod +x ~/.stopvnc.sh
```

## Configure nginx

Run the following commands to configure nginx:

```bash
sudo vim /etc/nginx/sites-enabled/default
```

Make sure the file looks like this:

```bash
server {
        listen 80;
        listen [::]:80;

        root /var/www/html;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        location / {
            proxy_pass http://127.0.0.1:6080/;
          }
          location /novnc/ {
            proxy_pass http://127.0.0.1:6080/;
          }
          location /novnc/websockify {
           proxy_pass http://127.0.0.1:6080/;
           proxy_http_version 1.1;
           proxy_set_header Upgrade $http_upgrade;
           proxy_set_header Connection "Upgrade";
           proxy_set_header Host $host;
         }
         location /websockify {
           proxy_pass http://127.0.0.1:6080/;
           proxy_http_version 1.1;
           proxy_set_header Upgrade $http_upgrade;
           proxy_set_header Connection "Upgrade";
           proxy_set_header Host $host;
         }

}

server {
        listen 443 ssl;
        listen [::]:443 ssl;

        root /var/www/html;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;
        ssl_certificate /etc/letsencrypt/live/fslcourse.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/fslcourse.com/privkey.pem;

        location / {
            proxy_pass http://127.0.0.1:6080/;
          }
          location /novnc/ {
            proxy_pass http://127.0.0.1:6080/;
          }
          location /novnc/websockify {
           proxy_pass http://127.0.0.1:6080/;
           proxy_http_version 1.1;
           proxy_set_header Upgrade $http_upgrade;
           proxy_set_header Connection "Upgrade";
           proxy_set_header Host $host;
         }
         location /websockify {
           proxy_pass http://127.0.0.1:6080/;
           proxy_http_version 1.1;
           proxy_set_header Upgrade $http_upgrade;
           proxy_set_header Connection "Upgrade";
           proxy_set_header Host $host;
         }
}
```

see this [guide](https://datawookie.dev/blog/2021/08/websockify-novnc-behind-an-nginx-proxy/) for more details

And this [guide](https://www.webhi.com/how-to/generate-lets-encrypt-wildcard-certificates-nginx/) for setting up a wildcard letsencrypt certificate for fslcourse.com

## Additional desktop configuration before creating the AMI

- disable screen lock
- disable screen saver
- set the desktop background to anything you like
- add firefox and the ternimal to the dock
- set the default browser to firefox
- set the firefox homepage to the FSL course website
- remove unused dock icons

All these changes are peristant and will be saved in the AMI.

## Make an AMI

Once the desktop is configured, create an AMI from the instance in the AWS web console. Use the AMI to provision new desktops using the terraform scripts in the `terraform` directory.

### Manually provisioning a new desktop using the AMI

The following steps are used to manually provision a desktop using the AMI. This method is useful when testing.

### Launch an instance using the AMI

Launch an instance using the AMI in the AWS web console. The instance type should be `t2.large` (2vCPU 8GB).

### Add user data in the advanced details (Form field)

Add the following user data script during provisioning and before launching the instance (THIS IS IMPORTANT!):

```bash
#!/bin/bash
# Check if the 'ubuntu' user exists, if not, create it
if ! id -u ubuntu > /dev/null 2>&1; then
    useradd -m -s /bin/bash ubuntu
    echo 'ubuntu:ubuntu' | chpasswd
fi
# Add the user to sudoers
echo "ubuntu ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ubuntu

# Optionally, customize further...
# see user_data.sh for a full example
```

### Start the VNC server

Connect to the instance using SSH and the `ubuntu` user and run the following commands to start the VNC server:

```bash
bash ~/.startvnc.sh
```

### Connect to the instance using your Web Browser

**Important**: You may get a warning when visiting the IP address of the instance in your web browser using `https`. This is because the certificate is for `fslcourse.com` and not the IP address. You can ignore this warning and proceed to the site. This warning is not present when using the domains created by the terraform scripts. You can avoid the warning by using `http` instead of `https` too.

If all goes well your instance should be accessible via a web browser at the public IP address of the instance. You can connect to the instance using the VNC password you set earlier.

You can find the public IP address of the instance in the AWS web console in the instance details section. You can also use the Public IPv4 DNS to connect to the instance.
